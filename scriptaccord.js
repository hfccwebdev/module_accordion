// jQuery(document).ready(function(){

    // $('body > div:nth-child(2)').addClass('sidenav');
    // $('body > div:nth-child(2)').attr('id', 'sideaccord');

    // $('#sideaccord > ul').addClass('accordion');
    // $('#sideaccord > ul').attr('id', 'mainaccordmenu');

    // add parent class and toggle class
    // $('#mainaccordmenu li ul').parent().addClass('parent');

//     alert("Hello! I am an alert box!!");

//     showMenu();

// });

(function ($) {
    $(document).ready(function(){
        $('.toggle').click(function() {

            // close menu item, set left arrow
            if ($(this).next('ul').hasClass('show')) {
                $(this).removeClass('arrowdown');
                $(this).attr("aria-selected","false");
                $(this).attr("aria-expanded","false");
                $(this).addClass('arrowleft');
                $(this).next('ul').removeClass('show');
                //$(this).next('ul').slideUp(350);
                $(this).parent().parent().removeClass('open');
            }

            // open menu item
            else {
                // close other items
                $('#mainaccordmenu').children('.parent').children('.toggle').removeClass('arrowdown');
                $('#mainaccordmenu').children('.parent').children('.toggle').addClass('arrowleft');
                $('#mainaccordmenu').children('.parent').children('.show').removeClass('show');
                $('#mainaccordmenu').children('.parent').children('.toggle').attr("aria-selected","false");
                $('#mainaccordmenu').children('.parent').children('.toggle').attr("aria-expanded","false");
                // open this item
                $(this).removeClass('arrowleft');
                $(this).addClass('arrowdown');
                $(this).attr("aria-expanded","true");
                $(this).attr("aria-selected","true");
                //$(this).parent().parent().find('.parent ul').slideUp(350);
                $(this).next('ul').addClass('show');
                //$(this).next('ul').slideToggle(350);
                $(this).parent().parent().addClass('open');
            }


         }); // end toggle.click

            // open menu
            $('#mainaccordmenu-open').click(function() {
                    if ($('#mainaccordmenu').hasClass('show')) {
                        $('#mainaccordmenu').removeClass('show');
                        $(this).attr("aria-expanded","false");
                        $(this).attr("aria-selected","false");
                        $(this).removeClass('open');
                        $('#mainaccordmenu').children('.parent').children('.toggle').removeClass('arrowdown');
                        $('#mainaccordmenu').children('.parent').children('.toggle').addClass('arrowleft');
                        $('#mainaccordmenu').children('.parent').children('.show').removeClass('show');
                        $('#mainaccordmenu').children('.parent').children('.toggle').attr("aria-selected","false");
                        $('#mainaccordmenu').children('.parent').children('.toggle').attr("aria-expanded","false");
                    }
                    else {
                        $('#mainaccordmenu').addClass('show');
                        $(this).attr("aria-expanded","true");
                        $(this).attr("aria-selected","true");
                        $(this).addClass('open');
                    }
            }); // end mainaccordmenu-open.click

            $('#mainaccordmenu').children().children().children().children().addClass('sub-item');

            $('.sub-item').focus(function() {
                $('#mainaccordmenu').children('.parent').children('.toggle').removeClass('arrowdown');
                $('#mainaccordmenu').children('.parent').children('.toggle').addClass('arrowleft');
                $('#mainaccordmenu').children('.parent').children('.show').removeClass('show');
                $('#mainaccordmenu').children('.parent').children('.toggle').attr("aria-selected","false");
                $('#mainaccordmenu').children('.parent').children('.toggle').attr("aria-expanded","false");
                $('#mainaccordmenu').children('.parent').children('.toggle').next('ul').removeClass('show');
                $(this).parent().parent().addClass('show');
                $(this).parent('li').parent('ul').parent('.parent').children('.toggle').removeClass('arrowleft');
                $(this).parent('li').parent('ul').parent('.parent').children('.toggle').addClass('arrowdown');
                $(this).parent('li').parent('ul').parent('.parent').children('.toggle').attr("aria-expanded","true");
                $(this).parent('li').parent('ul').parent('.parent').children('.toggle').attr("aria-selected","true");

                }); // end focus expand

    });
})(jQuery); // end showMenu function


// when one of the parent list items is toggled to reveal show go back up tree and toggle all of its siblings to be show
