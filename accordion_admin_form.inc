<?php

/**
 * @file
 * Contains the admin form.
 */

/**
 * Page callback for the settings form.
 */
function accordion_admin_form($form, &$form_state) {
  $counters = [];
  for ($i=0; $i < 21; $i++) {
    $counters[$i] = $i;
  }
  $form['accordion_menu_block_count'] = [
    '#type' => 'select',
    '#options' => $counters,
    '#title' => t('Number of bulletin blocks'),
    '#default_value' => variable_get('accordion_menu_block_count', 0),
    '#description' => t('The number of menu blocks.'),
  ];
  return system_settings_form($form);

}