<?php

/**
 * Callbacks for the Accordion module blocks.
 */
class AccordionBlock {

  /**
   * Displays the block menu.
   */
  public static function view($menu_name = 'main-menu') {

    $items = menu_tree_all_data($menu_name);

    // See https://api.drupal.org/comment/55718#comment-55718
    // for a way to populate in_active_trail for returned links.
    // They are not set by default.


    $output = '<div id="mainaccordmenu-open"><a href="#" class="no-link" aria-label="Mobile Menu button">Menu</a></div><ul class="menu" id="mainaccordmenu">';

    foreach ($items as $item) {

      if (!$item['link']['hidden']) {

        if (empty($item['below'])) {
          $output .= '<li>' . l($item['link']['link_title'], $item['link']['link_path']);
        }
        else {
          $output .= '<li class="parent">' . t('<a href="javascript:void(0);" class="arrow toggle arrowleft">@title</a>', ['@title' => $item['link']['link_title']]);
          $output .= '<ul>';
          foreach ($item['below'] as $child) {
            if (!$child['link']['hidden']) {
              $output .= '<li>';
              $output .= l($child['link']['link_title'], $child['link']['link_path']);
              $output .= '</li>';
            }
          }
          $output .= '</ul>';
        }

        $output .= '</li>';
      }
    }

    $output .= '</ul></li></ul>';

    return $output;

  }
}
